package lingnan.service;

import lingnan.entity.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Component
@FeignClient(value = "provider-hotel-8001")
public interface BookConsumerService {
    //    @PostMapping("/admin/queryAll")
//    Object queryAll(@RequestParam("page") Integer page, @RequestParam("limit")Integer limit, @RequestBody User bean);
//
//    @PostMapping("/admin/save")
//    public Object save(@RequestBody User bean);
//
//    @PostMapping("/admin/delete")
//    public boolean delete(@RequestBody Integer[] ids);
//    @GetMapping("/user/{id}")
//    public User getById(@PathVariable("id") Integer id);
    @GetMapping("/book/getById")
    Book getById(@RequestParam("bookId") Integer bookId);

    @GetMapping("/book/getListJson")
    List<Book> getListJson();

    @GetMapping(value = "/book/getMapJson")
    Map<String, Object> getMapJson();

    @PostMapping(value = "/book/findJson")
    Map<String, Object> findJson(@RequestParam("value") String value, @RequestParam("domain") String domain);

    @PostMapping(value = "/book/update")
    Object update(@RequestBody Book bean);

    @PostMapping(value = "/book/insert")
    Object insert(@RequestBody Book bean);

    @PostMapping(value="/book/deleteJson")
     String deleteJson(@RequestParam("bookId") Integer bookId);

    @PostMapping(value="/room/stockOne")
     boolean stockOne(@RequestParam("roomId") Integer roomId);


    @GetMapping("/book/getBookAllByStateUser")
    List<Book> getBookAllByStateUser(@RequestParam("bookState") int bookState,@RequestParam("userId") int userId);
    @GetMapping("/book/getBookAllByUser")
    List<Book> getBookAllByUser(@RequestParam("userId") int userId);


    @GetMapping(value = "book/bookEnd")
    boolean bookEnd(@RequestParam("bookId") int bookId);

    @PostMapping(value="/room/stockAdd")
    boolean stockAdd(@RequestParam("roomId") Integer roomId);
}
