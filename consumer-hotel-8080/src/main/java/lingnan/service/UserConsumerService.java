package lingnan.service;

import lingnan.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


/**
 * @author RIKU
 */
@Component
@FeignClient(value = "provider-hotel-8001")
public interface UserConsumerService {
//    @PostMapping("/admin/queryAll")
//    Object queryAll(@RequestParam("page") Integer page, @RequestParam("limit")Integer limit, @RequestBody User bean);
//
//    @PostMapping("/admin/save")
//    public Object save(@RequestBody User bean);
//
//    @PostMapping("/admin/delete")@PostMapping("/user/login")
//    public boolean delete(@RequestBody Integer[] ids);

    @PostMapping("/user/login")
    User login(@RequestParam("name") String name, @RequestParam("password") String password,  @RequestParam("isMemory") String isMemory);

    @PostMapping("/user/reg")
    String reg(@RequestBody User bean, @RequestParam("cfmPassword") String cfmPassword);

    @GetMapping("/user/getById/{id}")
    User getById(@PathVariable("id") Integer id);

    @GetMapping("/user/queryAll")
    Object queryAll();

    @PostMapping("/user/update")
    String update(@RequestBody User bean, @RequestParam("cfmPassword") String cfmPassword, @RequestParam("oldPassword") String oldPassword, @RequestParam("checkPhone") String checkPhone);

    @DeleteMapping("/user/deleteByIds/{ids}")
    Object deleteByIds(@PathVariable("ids") List<Integer> ids);

    @GetMapping("/user/getListJson")
    List<User> getListJson();

    @GetMapping(value="/user/getMapJson")
    Map<String, Object> getMapJson();

    @PostMapping("/user/findJson")
    Map<String, Object> findJson(@RequestParam("value") String value, @RequestParam("domain") String domain);

    @PostMapping("/user/deleteJson")
    String deleteJson(@RequestParam("id") Integer id);

    @RequestMapping(value="/user/updateGo")
    String updateGo(@RequestParam("id")int id,@RequestParam("name") String name,@RequestParam("password") String password
            ,@RequestParam("sex") String sex,@RequestParam("phone") String phone
            ,@RequestParam("email") String email,@RequestParam("access") int access);

    @PostMapping(value="/user/updatejson")
    boolean updatejson(@RequestBody User bean);

    @RequestMapping(value="/user/mine")
    String mine(@RequestParam("session") HttpSession session);

}
