package lingnan.service;


import lingnan.entity.Room;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Component
@FeignClient(value = "provider-hotel-8001")
public interface RoomConsumerService {
    @GetMapping("/room/getByRoomId/{roomId}")
    Room getByRoomId(@PathVariable("roomId") Integer roomId);

    @GetMapping("/room/queryAll")
    List<Room> queryAll();

    @RequestMapping("/room/save")
    void save(Room bean);

    @GetMapping("/room/findByType")
    List<Room> findByType(@RequestParam("roomType") String roomType);
   /* @PostMapping("/room/update")
    String update(Room bean);*/
 /*  @PostMapping("/room/update")
   Boolean update(Room bean);

    @PostMapping("/room/delete")
    String delete(Room bean);*/


    @GetMapping("/room/getMapJson")
    Map<String, Object> getMapJson();

    @PostMapping("/room/findJson")
    Map<String, Object> findJson(@RequestParam("value") String value, @RequestParam("domain") String domain);

    @PostMapping("/room/update")
    boolean update(@RequestBody Room bean);

    @PostMapping("/room/deleteJson")
    String deleteJson(@RequestParam("roomId") Integer roomId);


}
