package lingnan.controller;

import lingnan.entity.Book;
import lingnan.entity.User;
import lingnan.service.BookConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("book")
public class BookConsumerController {
    @Autowired
    private BookConsumerService bookConsumerService;



    //查询单个，返回一个对象
    @GetMapping(value = "/getById")
    @ResponseBody
    public Book getById(Integer bookId) {
        return bookConsumerService.getById(bookId);
    }
    //查询所有
    @GetMapping(value = "/getListJson")
    @ResponseBody
    public List<Book> getListJson() {
        return bookConsumerService.getListJson();
    }

    //上面为user.jsp实验用的方法，下面两个才是查询json方法  getMapjson为显示全部数据 ，findjson是条件查询输出数据
    @GetMapping(value = "/getMapJson")
    @ResponseBody
    public Map<String, Object> getMapJson() {
        return bookConsumerService.getMapJson();
    }

    @PostMapping(value = "/findJson")
    @ResponseBody
    public Map<String, Object> findJson(String value, String domain) {
        return bookConsumerService.findJson(value, domain);
    }

    //更新
    @PostMapping(value = "/update")
    @ResponseBody
    public Object update(Book bean) {
        System.out.println("bean:::"+bean);
        return bookConsumerService.update(bean);
//        return true;
    }

    //json执行删除操作
    @PostMapping(value = "/deleteJson")
    @ResponseBody
    public String deleteJson(Integer bookId) {
        return bookConsumerService.deleteJson(bookId);
    }

    //下面为页面跳转

    @RequestMapping(value="/updateGo")
    public String updateGo(Integer bookId, Model model){
        Book bean = bookConsumerService.getById(bookId);
        SimpleDateFormat sdf1 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String bookDate = sdf1.format(bean.getBookDate()); //2015-02-09 format()才是bai格du式化
        SimpleDateFormat sdf2 = new java.text.SimpleDateFormat("yyyy-MM-dd");
        String arriveDate = sdf2.format(bean.getArriveDate()); //2015-02-09 format()才是bai格du式化

        model.addAttribute("bookDate",bookDate);
        model.addAttribute("arriveDate",arriveDate);
        model.addAttribute("bean",bean);
        return "manage/managebookupdate";
    }

    @PostMapping("/confirm")
    public String confirm(String email, String bookPhone, @DateTimeFormat(pattern = "yyyy-MM-dd") Date arriveDate , String bookName, HttpSession session){
        Book bean=(Book) session.getAttribute("bookInf");
        User beanUser=(User) session.getAttribute("loginUser");
        System.out.println("bookInf:::"+bean);
        bean.setBookDate(new Date());
        System.out.println("BookDate下单时间:::"+bean.getBookDate());
        System.out.println("bookName:::"+bookName);
        bean.setUserId(beanUser.getId());
        bean.setArriveDate(arriveDate);
        bean.setBookState(1);
        bean.setBookPhone(bookPhone);
        bean.setEmail(email);
        bean.setBookName(bookName);

        boolean flag= (boolean) bookConsumerService.insert(bean);
        if(flag){
            session.setAttribute("bookInformation", bean);
            session.setAttribute("roomInformation", bookConsumerService.getById(bean.getRoomId()));
            if(bookConsumerService.stockOne(bean.getRoomId())){
                session.setAttribute("msg", "预定成功，请等待审核通知");
            } else {
                session.setAttribute("msg", "预定成功，库存出现错误，请等待审核通知");
            }
        }
        else{
            session.setAttribute("msg", "预定失败！");
            return "book/book";
        }
        return "success";
    }

    //客户退房
    @RequestMapping(value="/checkOut/{bookId}")
    public String checkOut(@PathVariable("bookId") Integer bookId, HttpSession session){
        boolean flag=false;
        flag=bookConsumerService.bookEnd(bookId);
        if(flag=false)
            return "error";
        System.out.println("delete:::"+flag);
        User user=(User) session.getAttribute("loginUser");
        int id=user.getId();
        int userId=user.getId();
        bookConsumerService.stockAdd(bookConsumerService.getById(bookId).getRoomId());
        session.setAttribute("allBook", bookConsumerService.getBookAllByUser(userId));
        session.setAttribute("checking", bookConsumerService.getBookAllByStateUser(1,id));
        session.setAttribute("outDate", bookConsumerService.getBookAllByStateUser(0,id));
        session.setAttribute("confirm", bookConsumerService.getBookAllByStateUser(2,id));
        System.out.println("allBook:::"+bookConsumerService.getBookAllByUser(userId));
        System.out.println("checking:::"+bookConsumerService.getBookAllByStateUser(1,id));
        System.out.println("outDate:::"+bookConsumerService.getBookAllByStateUser(0,id));
        System.out.println("confirm:::"+bookConsumerService.getBookAllByStateUser(2,id));
        System.out.println("退房成功");
        return "user/user";
    }

        @RequestMapping(value="/delete/{bookId}")
    public String delete(@PathVariable("bookId") Integer bookId,HttpSession session){

        String flag;
        flag=bookConsumerService.deleteJson(bookId);
        if(flag.equals("0")||flag==null)
            return "error";
        System.out.println("delete:::"+flag);
        User user=(User) session.getAttribute("loginUser");
            int id=user.getId();
            int userId=user.getId();
            session.setAttribute("allBook", bookConsumerService.getBookAllByUser(userId));
            session.setAttribute("checking", bookConsumerService.getBookAllByStateUser(1,id));
            session.setAttribute("outDate", bookConsumerService.getBookAllByStateUser(0,id));
            session.setAttribute("confirm", bookConsumerService.getBookAllByStateUser(2,id));
            System.out.println("allBook:::"+bookConsumerService.getBookAllByUser(userId));
            System.out.println("checking:::"+bookConsumerService.getBookAllByStateUser(1,id));
            System.out.println("outDate:::"+bookConsumerService.getBookAllByStateUser(0,id));
            System.out.println("confirm:::"+bookConsumerService.getBookAllByStateUser(2,id));
            System.out.println("删除成功");
            return "user/user";
    }


}
