package lingnan.controller;

import lingnan.entity.Book;
import lingnan.entity.Room;
import lingnan.service.RoomConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("room")
public class RoomConsumerController {
    @Autowired
    RoomConsumerService roomConsumerService;

    @GetMapping("toList")
    public ModelAndView toList() {
        ModelAndView mav = new ModelAndView("room/room_list");//页面路径
        List<Room> list = roomConsumerService.queryAll();//查询全部的方法
        mav.addObject("list", list);
        return mav;
    }

    @GetMapping("toFind")
    public String toFind(){
        return "room/room_list";
    }
    //通过房间类型进行模糊查询
    @GetMapping("findByType")
    public  String findByType(HttpSession session,Room bean){
        System.out.println("bean.getRoomType():::"+bean.getRoomType());
        List<Room> lists =  roomConsumerService.findByType(bean.getRoomType());
        session.setAttribute("lists", lists);
        System.out.println(lists);
        return "room/room_list";
    }

    @GetMapping("/getByRoomId/{roomId}")
    @ResponseBody
    public Room getByRoomId(@PathVariable("roomId") Integer roomId) {
        return roomConsumerService.getByRoomId(roomId);
    }
/*
    @GetMapping("/lists")
    @ResponseBody
    public String getByType(HttpSession session, String roomType, Room bean){
        System.out.println("roomType:::"+roomType);
        List<Room> lists =  roomConsumerService.getByType(bean);
        session.setAttribute("lists", lists);
        System.out.println(lists);
        return "room/room_list";
    }*/

    @GetMapping("/queryAll")
    @ResponseBody
    public Object queryAll() {
        return roomConsumerService.queryAll();
    }

    @RequestMapping("/save")
    @ResponseBody
    public void save(Room bean) {
        roomConsumerService.save(bean);
    }


/*

    @PostMapping("/update")
    @ResponseBody
    public Boolean update(Room bean){
        return roomConsumerService.update(bean);
    }

    @PostMapping("/delete")
    @ResponseBody
    public String delete(Room bean){
        return roomConsumerService.delete(bean);
    }
*/


    //上面为room.jsp实验用的方法，下面两个才是查询json方法  getMapjson为显示全部数据 ，findjson是条件查询输出数据
    @GetMapping("/getMapJson")
    @ResponseBody
    public Map<String, Object> getMapJson() {
        return roomConsumerService.getMapJson();
    }

    @PostMapping("/findJson")
    @ResponseBody
    public Map<String, Object> findJson(String value, String domain) {
        return roomConsumerService.findJson(value, domain);
    }

    //更新
    @PostMapping("/update")
    @ResponseBody
    public boolean update(Room bean) {
        System.out.println(bean);
        return roomConsumerService.update(bean);
    }

    //json执行删除操作
    @PostMapping("/deleteJson")
    @ResponseBody
    public String deleteJson(Integer roomId) {
        return roomConsumerService.deleteJson(roomId);
    }

    @RequestMapping(value = "/updateGo")
    public String updateGo(Integer roomId, Model model) {
        Room bean = roomConsumerService.getByRoomId(roomId);
        model.addAttribute("bean", bean);
        return "manage/manageroomupdate";
    }

    //跳转到预定界面
    @RequestMapping("gobook")
    public String gobook(@RequestParam("roomId") int roomId, @RequestParam("roomStock") int roomStock, HttpSession session) {
        System.out.println("roomStock:::" + roomStock);
        if (roomStock <= 0)
            return "error";
        Book book = new Book();
//			User user=(User) session.getAttribute("loginUser");
//			System.out.println("loginUser:::"+user);
        Room bean = roomConsumerService.getByRoomId(roomId);
//			Book book = null;
        book.setRoomId(bean.getRoomId());
//			book.setUserId(user.getId());
        book.setBookPrice(bean.getRoomPrice());
        session.setAttribute("bookInf", book);
        session.setAttribute("room", bean);
        return "book/book";
    }

}
