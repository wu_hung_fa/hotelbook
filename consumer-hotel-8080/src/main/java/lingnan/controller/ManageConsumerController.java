package lingnan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author whongf
 * @create 2020-06-13-20:15
 */

@Controller
@RequestMapping("manage")
public class ManageConsumerController {

    @RequestMapping(value = "/managemain")
    public String managemain() {
        return "manage/manage";
    }

    @RequestMapping(value = "/manageuser")
    public String manageuser() {
        return "manage/manageuser";
    }

    @RequestMapping(value = "/managebook")
    public String managebook() {
        return "manage/managebook";
    }

    @RequestMapping(value = "/manageroom")
    public String manageroom() {
        return "manage/manageroom";
    }


}