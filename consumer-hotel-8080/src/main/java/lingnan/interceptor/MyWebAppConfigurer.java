package lingnan.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by gonghao on 2017/6/3.
 */
@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter{

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/user/toLogin").setViewName("login");
    }
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new RequestLog()).addPathPatterns("/**");
//        super.addInterceptors(registry);
//    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")
        .excludePathPatterns("/user/toLogin")
        .excludePathPatterns("/user/toReg")
        .excludePathPatterns("/assets/**")
        .excludePathPatterns("/css/**")
        .excludePathPatterns("/fonts/**")
        .excludePathPatterns("/images/**")
        .excludePathPatterns("/js/**")
        .excludePathPatterns("/layui/**")
        .excludePathPatterns("/static/**");
        super.addInterceptors(registry);
    }
}