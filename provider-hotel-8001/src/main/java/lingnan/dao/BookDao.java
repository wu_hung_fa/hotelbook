package lingnan.dao;

import lingnan.entity.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (Book)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-13 09:12:21
 */
@Mapper
@Repository
public interface BookDao {

    List<Book> getBookAllByStateNotZero ();
    List<Book> getBookAllByState(int bookState);
    List<Book> getBookAllByStateUser(@Param("bookState")int bookState,@Param("userId")int userId);
//    List<Book> getBookuserByState(int bookState);
    List<Book> getBookAll();
    List<Book> getBookAllByUser(int userId);
    int countAllBook();
    List<Book> getListJson();
    List<Book> getListDeleteJson(int bookId);
    List<Book> findByAll(Book bean);
    List<Book> getBookByUserId(int userId);
    Book getBookByBookId(int bookId);
    boolean updateByBookId(Book book);

    boolean deleteByBoodId(int id);
    boolean insertBook(Book bean);

    Book getById(Integer bookId);
}