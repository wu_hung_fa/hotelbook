package lingnan.service;

import lingnan.entity.Book;
import java.util.List;

/**
 * (Book)表服务接口
 *
 * @author makejava
 * @since 2020-06-13 09:12:21
 */
public interface BookService {

    List<Book> getBookAllByStateNotZero ();
    List<Book> getBookAllByState(int bookState);
    List<Book> getBookAllByStateUser(int bookState,int userId);
    List<Book> getBookAll();
    List<Book> getBookAllByUser(int userId);
    List<Book> getListJson();
    List<Book> getListDeleteJson(int bookId);
    List<Book> findByAll(Book bean);
    List<Book> getBookByUserId(int userId);
    int countAllBook();
    Book getBookByBookId(int bookId);
    boolean updateByBookId(Book bean);

    boolean bookSuccess(int bookId);
    boolean bookEnd(int bookId);

    boolean deleteByBoodId(int id);
    boolean insertBook(Book bean);

    Book getById(Integer bookId);
}