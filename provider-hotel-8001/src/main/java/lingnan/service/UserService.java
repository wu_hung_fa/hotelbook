package lingnan.service;

import lingnan.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (User)表服务接口
 *
 * @author makejava
 * @since 2020-06-12 17:08:13
 */
public interface UserService {

    /**
     * @param name 用户名/账号
     * @param password  密码
     * @return 实例对象
     */
    User login(String name, String password);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id);

    /**
     * 通过用户名查询其是否已存在
     *
     * @param name 用户名
     * @return 用户名
     */
    User queryByName(String name);

    /**
     * 查询所有对象
     *
     * @return 所有对象
     */
    List<User> queryAll();

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<User> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 是否成功
     */
    boolean insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 是否成功
     */
    boolean update(User user);

    /**
     * 更新最后登录时间
     *
     * @param bean 用户对象
     * @return 是否成功
     */
    boolean updateLoginDate(User bean);

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 影响行数
     */
    int deleteByIds(List<Integer> ids);


    //未知用途的方法：

    List<User> findByAll(User user);

    int countAllUser();

}