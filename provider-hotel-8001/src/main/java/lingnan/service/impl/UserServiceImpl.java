package lingnan.service.impl;

import lingnan.entity.User;
import lingnan.dao.UserDao;
import lingnan.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author makejava
 * @since 2020-06-12 17:08:13
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    /**
     * 通过用户名和密码登录
     *
     * @param name 用户名
     * @param password 密码
     * @return 用户对象
     */
    @Override
    public User login(String name, String password) {
        return this.userDao.login(name,password);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public User queryById(Integer id) {
        return this.userDao.queryById(id);
    }

    /**
     * 通过用户名查询其是否已存在
     *
     * @param name 用户名
     * @return 用户名
     */
    @Override
    public User queryByName(String name) {
        return this.userDao.queryByName(name);
    }

    /**
     * 查询所有对象
     *
     * @return 所有对象
     */
    @Override
    public List<User> queryAll() {
        return this.userDao.queryAll();
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<User> queryAllByLimit(int offset, int limit) {
        return this.userDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(User user) {
        return this.userDao.insert(user);
    }


    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(User user) {
        return this.userDao.update(user);
    }

    /**
     * 更新最后登录时间
     *
     * @param bean 用户对象
     * @return 是否成功
     */
    @Override
    public boolean updateLoginDate(User bean) {
        return this.userDao.updateLoginDate(bean);
    }

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    @Override
    public int deleteByIds(List<Integer> ids) {
        return this.userDao.deleteByIds(ids);
    }



    //未知用途的方法：

    @Override
    public List<User> findByAll(User user) {
        return this.userDao.findByAll(user);
    }

    @Override
    public int countAllUser() {
        return this.userDao.countAllUser();
    }
}