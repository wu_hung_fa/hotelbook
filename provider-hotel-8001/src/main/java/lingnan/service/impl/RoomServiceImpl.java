package lingnan.service.impl;

import lingnan.dao.RoomDao;
import lingnan.entity.Room;
import lingnan.service.RoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("roomService")
public class RoomServiceImpl implements RoomService {
    
	@Resource
	private RoomDao roomDao;
	
	public void save(Room bean) {
		roomDao.save(bean);
	}

	
	public Room getByRoomId(Integer roomId) {
	
		return roomDao.getByRoomId(roomId);
	}
	
	public boolean update(Room bean) {
		
		return roomDao.update(bean);
	}

	public boolean delete(Integer roomId) {
		
		return roomDao.delete(roomId);
	}

	public List<Room> queryAll() {
		
		return roomDao.queryAll();
	}


	//通过模糊查询类型
	public List<Room> findByType(Room bean) {
	
		return roomDao.findByType(bean);
	}


	public int countAllRoom() {
		return roomDao.countAllRoom();
	}


	public List<Room> findByAll(Room bean) {
		return roomDao.findByAll(bean);
	}


	public boolean stockOne(int roomId) {
		return roomDao.stockOne(roomId);
	}
	public boolean stockAddOne(int roomId) {
		return roomDao.stockAddOne(roomId);
	}

}
