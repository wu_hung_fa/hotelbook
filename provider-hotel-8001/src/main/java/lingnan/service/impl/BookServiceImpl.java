package lingnan.service.impl;

import lingnan.entity.Book;
import lingnan.dao.BookDao;
import lingnan.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Book)表服务实现类
 *
 * @author makejava
 * @since 2020-06-13 09:12:21
 */
@Service("bookService")
public class BookServiceImpl implements BookService {
    @Autowired
    private BookDao bookDao;

    @Resource
    private Book book;

    public List<Book> getBookAllByStateNotZero() {
        return bookDao.getBookAllByStateNotZero();
    }

    public Book getBookByBookId(int bookId) {
        return bookDao.getBookByBookId(bookId);
    }

    public boolean updateByBookId(Book bean) {
        System.out.println("bean:::"+bean);
        return bookDao.updateByBookId(bean);
    }

    public boolean deleteByBoodId(int id) {
        return bookDao.deleteByBoodId(id);
    }

    public boolean insertBook(Book bean) {
        return bookDao.insertBook(bean);
    }

    @Override
    public Book getById(Integer bookId) {
        return bookDao.getById(bookId);
    }

    public boolean bookSuccess(int bookId) {
        return true;

    }

    public boolean bookEnd(int bookId) {
        book.setBookId(bookId);
        book.setBookState(0);
        return bookDao.updateByBookId(book);
    }

    public List<Book> getBookAllByState(int state) {
        return  bookDao.getBookAllByState(state);
    }

    public List<Book> getBookAll() {
        // TODO Auto-generated method stub
        return bookDao.getBookAll();
    }

    public List<Book> getListJson() {
        // TODO Auto-generated method stub
        return bookDao.getListJson();
    }

    public int countAllBook() {
        return bookDao.countAllBook();
    }

    public List<Book> findByAll(Book book) {
        return bookDao.findByAll(book);
    }

    public List<Book> getListDeleteJson(int bookId) {
        return bookDao.getListDeleteJson(bookId);
    }

    public List<Book> getBookByUserId(int userId) {
        return bookDao.getBookByUserId(userId);
    }

    public List<Book> getBookAllByStateUser(int bookState,int userId) {
        return bookDao.getBookAllByStateUser(bookState, userId);
    }

    public List<Book> getBookAllByUser(int userId) {
        return bookDao.getBookAllByUser(userId);
    }
}