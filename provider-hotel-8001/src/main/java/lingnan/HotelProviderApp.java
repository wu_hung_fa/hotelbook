package lingnan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class HotelProviderApp {
    public static void main(String[] args) {
        SpringApplication.run(HotelProviderApp.class, args);
    }
}
