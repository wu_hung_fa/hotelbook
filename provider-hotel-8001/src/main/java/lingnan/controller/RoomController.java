package lingnan.controller;

//import lingnan.entity.Book;

import lingnan.entity.Room;
import lingnan.service.RoomService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Controller
@RequestMapping("room")
public class RoomController {
	@Resource
	private RoomService roomService;
	@Resource
	private  Room bean;

	@GetMapping("getByRoomId/{roomId}")
	@ResponseBody
	public Room getByRoomId(@PathVariable("roomId") Integer roomId){
		Room room = roomService.getByRoomId(roomId);
		if(room == null){
			throw new RuntimeException("出错了，没有该roomId");
		}
		return room;
	}
	/*@GetMapping("queryAll")
	public Object queryAll(){
		return roomService.queryAll();
	}*/
	//查询所有
	@GetMapping("queryAll")
	@ResponseBody
	 public Object queryAll(HttpServletRequest request){
		 List<Room> list = roomService.queryAll();
		 request.setAttribute("list", list);
		 return list;
	   //  return "forward:../room.jsp";
	 }
	@GetMapping("/getListJson")
	@ResponseBody
	 public List<Room> getListJson() {
        System.out.println("getListJson");
        return roomService.queryAll();
    }
 
	/* //修改
	 @PostMapping("update")
	 @ResponseBody
	 public String update(HttpSession session, Room bean){
			boolean flag= false;
			flag = roomService.update(bean);
			if(flag=false)
				return "error";
			System.out.println("update: "+flag);
			session.setAttribute("update", roomService.queryAll());
			System.out.println(bean);

		    return "redirect:list";
	 }*/
	/*
	 //更新
	 @PostMapping("delete")
	 @ResponseBody
	 public String delete(HttpSession session, Room bean, Integer roomId){
	     boolean flag = false;
	     flag = roomService.delete(roomId);
	     if(flag = false)
	    	 return "error";
	     System.out.println("delete: "+flag);
	     session.setAttribute("delete", roomService.queryAll());
	     System.out.println(bean);
		 return "room";
	 }*/
	 
	 @GetMapping("/findById")
	 @ResponseBody
	    public Object findById(@RequestParam(value = "roomId") Integer roomId, Model model) {
        System.out.println("roomId:" + roomId);
        Room room = roomService.getByRoomId(roomId);  
        model.addAttribute("room", room);
        return room;
    }
	 
	 //通过房间类型进行模糊查询
	 @GetMapping("findByType")
	 @ResponseBody
	 public  List<Room> findByType(@RequestParam("roomType") String roomType){
	 	Room bean = new Room();
	 	 bean.setRoomType(roomType);
		 List<Room> lists =  roomService.findByType(bean);
	    // session.setAttribute("lists", lists);
	     System.out.println(lists);
		 return lists;
	 }
	 
	 //增加room数据，并上传图片
	@RequestMapping("/save")
	@ResponseBody
	public String upload(Room bean, HttpServletRequest request, Model model) throws IllegalStateException, IOException {
		System.out.println(request.getParameter("roomType"));
		//保存数据库路径
		String sqlPath = null;
		//定义文件保存的本地路径
		String localPath="C:\\upload\\";
		//定义文件名
		String filename = null;
		if(!bean.getFile().isEmpty()){
			//生成uuid作为文件名称
			String uuid = UUID.randomUUID().toString().replaceAll("-","");
			//获得文件类型
			String contenType = bean.getFile().getContentType();
			//获取文件后缀
			String suffixName = contenType.substring(contenType.indexOf("/")+1);
			//得到文件名
			filename = uuid+"."+suffixName;
			System.out.println(filename);
			//保存文件路径
			bean.getFile().transferTo(new File(localPath+filename));
		  }
			sqlPath = filename;
			System.out.println(sqlPath);
			bean.setRoomId(Integer.parseInt(request.getParameter("roomId")));
			bean.setRoomType(request.getParameter("roomType"));
			bean.setRoomPrice(Integer.parseInt(request.getParameter("roomPrice")));
			bean.setRoomStock(Integer.parseInt(request.getParameter("roomStock")));
			bean.setImage(sqlPath);
			bean.setRemark(request.getParameter("remark"));
			roomService.save(bean);
			model.addAttribute("bean", bean);
			System.out.println(bean);
			return "redirect:list";
		}
	

	//json查询下面两个是查询json方法  getMapjson为显示全部数据 ，findjson是条件查询输出数据
	@GetMapping(value="/getMapJson")
	@ResponseBody
	public Map<String, Object> getMapJson(){
		 List<Room> rooms = roomService.queryAll();
	        int count = roomService.countAllRoom();
	        System.out.println("coun:::"+count);
	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("msg","");
	        map.put("code",0);
	        map.put("count",count);
	        map.put("data",rooms);
	        System.out.println(map);
	        return map;
	}
	
	@PostMapping(value="/findJson")
	@ResponseBody
	public Map<String, Object> findJson(@RequestParam("value") String value, @RequestParam("domain") String domain){
		Room bean=new Room();
	 	System.out.println("value:::"+value);
		System.out.println("domain:::"+domain);
		System.out.println("类型是：：："+domain.getClass().getName());
		if(domain.equals("0"))
		bean.setRoomId(Integer.parseInt(value));

		
		if(domain.equals("1"))
		bean.setRoomType(value);
		if(domain.equals("2"))
		bean.setRoomPrice(Integer.parseInt(value));
		if(domain.equals("3"))
		bean.setRoomStock(Integer.parseInt(value));
		if(domain.equals("4"))
		bean.setImage(value);
		if(domain.equals("5"))
		bean.setRemark(value);
		
		
		 List<Room> rooms = roomService.findByAll(bean);
	        int count = roomService.countAllRoom();
	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("msg","");
	        map.put("code",0);
	        map.put("count",count);
	        map.put("data",rooms);
	        System.out.println(map);
	        return map;
	}
	//json删除
	//json执行删除操作
		@PostMapping(value="/deleteJson")
		@ResponseBody
		public String deleteJson(@RequestParam("roomId") Integer roomId){
			//删除要用查找 找到这个值返回整条数据 外面会输出1表示有数据 然后在外面删除
			boolean flag=false;
			flag = roomService.delete(roomId);
			if(flag==true)
				return "1";
			return "0";
		}
		
	/*	//json更新
		//下面两个为json更新 updateGo为页面跳转显示更新页面，update是执行更新操作
		@RequestMapping(value="/updateGo")
		public String updateGo(@RequestParam("roomId")int roomId, @RequestParam("roomType") String roomType
				, @RequestParam("roomPrice") int roomPrice, @RequestParam("roomStock") int roomStock
				, @RequestParam("image") String image, @RequestParam("remark") String remark){

			return "manageroomupdate";
		}*/
		
		@PostMapping(value="/update")
		@ResponseBody
		public boolean update(@RequestBody Room bean){
			
			return roomService.update(bean);
		}


		//下订单后减1库存
	@PostMapping(value="/stockOne")
	@ResponseBody
	public boolean stockOne(@RequestParam("roomId") Integer roomId){
		return roomService.stockOne(roomId);
	}
		//下订单后加1库存
	@PostMapping(value="/stockAdd")
	@ResponseBody
	public boolean stockAdd(@RequestParam("roomId") Integer roomId){
		return roomService.stockAddOne(roomId);
	}

}
