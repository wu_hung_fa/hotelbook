package lingnan.controller;

import lingnan.entity.Book;
import lingnan.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Book)表控制层
 *
 * @author makejava
 * @since 2020-06-13 09:12:21
 */
@RestController
@RequestMapping("book")
public class BookController {
    @Autowired
    private BookService bookService;

    @Resource
    private  Book bean;
//    @Autowired
//    private RoomService roomService;




//
//    @RequestMapping(value="/delete")
//    public String delete(@RequestParam("bookId") Integer bookId,HttpSession session){
//
//        boolean flag=false;
//        flag=bookService.deleteByBoodId(bookId);
//        if(flag=false)
//            return "error";
//        System.out.println("delete:::"+flag);
//        User user=(User) session.getAttribute("loginUser");
//        int id=user.getId();
//        session.setAttribute("allBook", bookService.getBookAllByUser(id));
//        session.setAttribute("checking", bookService.getBookAllByStateUser(1,id));
//        session.setAttribute("outDate", bookService.getBookAllByStateUser(0,id));
//        session.setAttribute("confirm", bookService.getBookAllByStateUser(2,id));
//        System.out.println("allBook:::"+bookService.getBookAllByUser(id));
//        System.out.println("checking:::"+bookService.getBookAllByStateUser(1,id));
//        System.out.println("outDate:::"+bookService.getBookAllByStateUser(0,id));
//        System.out.println("confirm:::"+bookService.getBookAllByStateUser(2,id));
//        return "user";
//    }
//    @RequestMapping(value="/checkOut")
//    public String checkOut(@RequestParam("bookId") Integer bookId,HttpSession session){
//        boolean flag=false;
//        flag=bookService.bookEnd(bookId);
//        if(flag=false)
//            return "error";
//        System.out.println("delete:::"+flag);
//        User user=(User) session.getAttribute("loginUser");
//        int id=user.getId();
//        roomService.stockAddOne(bookService.getBookByBookId(bookId).getRoomId());
//        session.setAttribute("allBook", bookService.getBookAllByUser(id));
//        session.setAttribute("checking", bookService.getBookAllByStateUser(1,id));
//        session.setAttribute("outDate", bookService.getBookAllByStateUser(0,id));
//        session.setAttribute("confirm", bookService.getBookAllByStateUser(2,id));
//        System.out.println("allBook:::"+bookService.getBookAllByUser(id));
//        System.out.println("checking:::"+bookService.getBookAllByStateUser(1,id));
//        System.out.println("outDate:::"+bookService.getBookAllByStateUser(0,id));
//        System.out.println("confirm:::"+bookService.getBookAllByStateUser(2,id));
//        return "user";
//    }
//    @RequestMapping(value="/update")
//    public String update(@RequestParam("bookId") Integer bookId,@RequestParam("book") Book book,HttpSession session){
//
//        boolean flag=false;
//        flag=bookService.updateByBookId(book);
//        if(flag=false)
//            return "error";
//        System.out.println("update:::"+flag);
//        User user=(User) session.getAttribute("loginUser");
//        int id=user.getId();
//        session.setAttribute("allBook", bookService.getBookAllByUser(id));
//        session.setAttribute("checking", bookService.getBookAllByStateUser(1,id));
//        session.setAttribute("outDate", bookService.getBookAllByStateUser(0,id));
//        session.setAttribute("confirm", bookService.getBookAllByStateUser(2,id));
//        System.out.println("allBook:::"+bookService.getBookAllByUser(id));
//        System.out.println("checking:::"+bookService.getBookAllByStateUser(1,id));
//        System.out.println("outDate:::"+bookService.getBookAllByStateUser(0,id));
//        System.out.println("confirm:::"+bookService.getBookAllByStateUser(2,id));
//        return "user";
//    }

    @GetMapping(value="/getById")
    @ResponseBody
    public Book getById(Integer bookId){
        System.out.println("getListJson");
        return bookService.getById(bookId);
    }

    @GetMapping(value="/getListJson")
    @ResponseBody
    public List<Book> getListJson(){
        System.out.println("getListJson");
        return bookService.getListJson();
    }



    //上面为user.jsp实验用的方法，下面两个才是查询json方法  getMapjson为显示全部数据 ，findjson是条件查询输出数据
    @GetMapping(value="/getMapJson")
    @ResponseBody
    public Map<String, Object> getMapJson(){
        List<Book> books = bookService.getListJson();
        int count = bookService.countAllBook();
        System.out.println("coun:::"+count);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","");
        map.put("code",0);
        map.put("count",count);
        map.put("data",books);
        System.out.println(map);
        return map;
    }

    @PostMapping(value="/findJson")
    @ResponseBody
    public Map<String, Object> findJson(@RequestParam("value") String value,@RequestParam("domain") String domain){
        Book bean=new Book();
        System.out.println("value:::"+value);
        System.out.println("domain:::"+domain);
        System.out.println("类型是：：："+domain.getClass().getName());
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("application-context.xml");
//        Book bean=ctx.getBean("book",Book.class);
        if(domain.equals("0"))
            bean.setBookId(Integer.parseInt(value));

        if(domain.equals("1"))
            bean.setUserId(Integer.parseInt(value));
        if(domain.equals("2"))
            bean.setRoomId(Integer.parseInt(value));
        if(domain.equals("3"))
            bean.setBookPhone(value);
        if(domain.equals("4"))
            bean.setEmail(value);
        if(domain.equals("5"))
            bean.setBookName(value);
        if(domain.equals("6"))
            bean.setBookPrice(Integer.parseInt(value));
        if(domain.equals("7"))
            bean.setBookState(Integer.parseInt(value));
        System.out.println("bean:::"+bean);
        List<Book> books = bookService.findByAll(bean);
        System.out.println("books:::"+books);
        int count = bookService.countAllBook();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg","");
        map.put("code",0);
        map.put("count",count);
        map.put("data",books);
        System.out.println(map);
        return map;
    }


    /*	@PostMapping(value="/getListDeleteJson")
        @ResponseBody
        public String getListDeleteJson(@RequestParam("bookId") Integer bookId){
             List<Book> books = bookService.getListDeleteJson(bookId);
             String state="0";
               if(books!=null)
                   state="1";
                return state;
        }	*/
    //json执行删除操作
    @PostMapping(value="/deleteJson")
    @ResponseBody
    public String deleteJson(@RequestParam("bookId") Integer bookId){
        //删除要用查找 找到这个值返回整条数据 外面会输出1表示有数据 然后在外面删除
        boolean flag=false;
        flag = bookService.deleteByBoodId(bookId);
        if(flag==true)
            return "1";
        return "0";
    }


    @GetMapping(value = "/getBookAllByStateUser")
    @ResponseBody
    public List<Book> getBookAllByStateUser(@RequestParam("bookState") int bookState,@RequestParam("userId") int userId){
        return bookService.getBookAllByStateUser(bookState,userId);
    }

    @GetMapping(value = "/getBookAllByUser")
    @ResponseBody
    List<Book> getBookAllByUser(@RequestParam("userId") int userId){
        return bookService.getBookAllByUser(userId);
    }
    @GetMapping(value = "/bookEnd")
    @ResponseBody
    boolean bookEnd(@RequestParam("bookId") int bookId){
        return bookService.bookEnd(bookId);
    }
    /*@GetMapping(value="/updateJson")
    @ResponseBody
    public boolean updateJson(Book bean){
        boolean flag=bookService.updateByBookId(bean);
        return flag;
    }*/
    //下面两个为json更新 updateGo为页面跳转显示更新页面，update是执行更新操作
//    @RequestMapping(value="/updateGo")
//    public String updateGo(@RequestParam("bookId")int bookId,@RequestParam("roomId") int roomId,@RequestParam("userId") int userId
//            ,@RequestParam("bookPhone") String bookPhone,@RequestParam("email") String email,@RequestParam("bookPrice") int bookPrice,@RequestParam("bookState") int bookState
//            ,@RequestParam("bookName") String bookName
//            ,@RequestParam("arriveDate") String arriveDate){
//
//        return "managebookupdate";
//    }

    @PostMapping(value="/update")
    @ResponseBody
    public Object update(@RequestBody Book bean){

        return bookService.updateByBookId(bean);
    }
    @PostMapping(value="/insert")
    @ResponseBody
    public Object insert(@RequestBody Book bean){

        return bookService.insertBook(bean);
    }







	/*@RequestMapping(value="/manageuser")
	public String manageuser(){
		return "manageuser";
	}*/



}