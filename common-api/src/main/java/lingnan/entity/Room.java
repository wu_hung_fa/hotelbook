package lingnan.entity;

import lingnan.annotation.Id;
import lingnan.annotation.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Data
@ToString
@Accessors
@NoArgsConstructor
@AllArgsConstructor
@Table("hotelbook.room")
@Component
public class Room {
	@Id
    private Integer roomId;
	private String roomType;
	private Integer roomPrice;
	private Integer roomStock;
	private String image;
	private String remark;
	private MultipartFile file;

}
