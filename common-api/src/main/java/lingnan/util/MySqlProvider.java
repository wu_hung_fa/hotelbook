package lingnan.util;

import lingnan.annotation.Table;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 */
public class MySqlProvider {
    public static final String INSERT = "insert";
    public static final String DELETE = "delete";
    public static final String UPDATE = "update";
    public static final String SELECT = "select";

    //插入
    public static String insert(Object obj) {
        String table = getTableName(obj);
        System.out.println(table);
        Map<String, String> map = getMap(obj);

        return new SQL(){
            {
                //insert into table() values()
                INSERT_INTO(table);//insert into table
                for (String key : map.keySet()) {
                    System.out.println("key:::"+key+",map.get(key)::"+map.get(key));
                    VALUES(key, map.get(key));
                }
            }
        }.toString();
    }
    //更新
    public static String update(Object obj) {
        String table = getTableName(obj);
        System.out.println(table);
        Map<String, String> map = getMap(obj);


        return new SQL(){
            {
                //insert into table() values()
                UPDATE(table);//insert into table
                for (String key : map.keySet()) {
                    if (key.equals("id")){
                        WHERE("id="+map.get(key));
                        continue;
                    }
                    System.out.println("key:::"+key+",map.get(key)::"+map.get(key));
                    String setValue=key+"="+map.get(key);
                    SET(setValue);
                }
            }
        }.toString();
    }
    //删除
    public static String delete(@Param("table") String table, @Param("where") String where) {
        System.out.println(table+":::"+where);
        if (StringUtil.isEmpty(table)) {
            return null;
        }

        if (StringUtil.isEmpty(where)) {
            return null;
        }

        return new SQL(){
            {
                DELETE_FROM(table);
                WHERE(where);
            }
        }.toString();
    }

    //分页查询
//    public String select(Cat cat) {
//        return new SQL() {
//
//            {
//                SELECT("id,cat_name,cat_age");
//                FROM("cat");
//                if (cat.getCat_name() != null) {
//                    WHERE("cat_name = #{cat_name}");
//                } else if (cat.getCat_age() != 0) {
//                    WHERE("cat_age = #{cat_age}");
//                }
//            }
//        }.toString();
//    }

    /**
     * 获取表名称
     * @param obj
     */
    private static String getTableName(Object obj) {
        Class c = obj.getClass();
        Table table = (Table)c.getAnnotation(Table.class);
        if (table != null) {
            return table.value();
        }

        return c.getSimpleName();
    }

    private static Map<String, String> getMap(Object obj) {
        Map<String, String> map = new HashMap<>();
        Class c = obj.getClass();
        Field[] fs = c.getDeclaredFields();//获取所有属性
        for (Field item : fs) {
            String key = item.getName();
            item.setAccessible(true);
            try {
                if (item.get(obj) != null) {
                    map.put(StringUtil.underscoreName(key), "#{" + key + "}");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return map;
    }

}
